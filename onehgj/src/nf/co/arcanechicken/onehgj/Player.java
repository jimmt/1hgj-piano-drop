package nf.co.arcanechicken.onehgj;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;

public class Player extends Image {
	private Body body;
	private Array<Bullet> bullets;
	private float width = 1f, height = 2f, lastShotTime = 99999999f, shotCap = 0.5f;
	private Vector2 temp;
	private World world;
	public boolean gameOver = false;

	public Player(World world) {
		super(Textures.player);

		bullets = new Array<Bullet>();
		initBody(world);
		this.world = world;

		temp = new Vector2();

	}

	private void initBody(World world) {
		PolygonShape hitbox = new PolygonShape();
		hitbox.setAsBox(width / 2, height / 2);

		BodyDef bd = new BodyDef();
		bd.type = BodyType.DynamicBody;
		bd.fixedRotation = true;
		bd.gravityScale = 0.0f;

		body = world.createBody(bd);

		FixtureDef fd = new FixtureDef();
		fd.shape = hitbox;
		fd.friction = 10.0f;
		fd.density = 1.0f;
		body.createFixture(fd);
		body.setUserData(this);
		body.setTransform(0, 1.0f, 0);

		setPosition(body.getPosition().x - width / 2, body.getPosition().y - height / 2);

		setSize(width, height);
	}

	public void act(float delta) {
		super.act(delta);

		setPosition(body.getPosition().x - width / 2, body.getPosition().y - height / 2);
		if (Gdx.input.isKeyPressed(Keys.A) || Gdx.input.isKeyPressed(Keys.D)) {

			if (Gdx.input.isKeyPressed(Keys.A)) {
				body.setLinearVelocity(temp.set(-200 * delta, body.getLinearVelocity().y));
			}
			if (Gdx.input.isKeyPressed(Keys.D)) {
				body.setLinearVelocity(temp.set(200 * delta, body.getLinearVelocity().y));
			}
		} else {
			body.setLinearVelocity(0, 0);
		}
		if (Gdx.input.isButtonPressed(Buttons.LEFT) && lastShotTime >= shotCap) {
			fire();
			lastShotTime = 0;
		} else {
			lastShotTime += delta;
		}
		if (body.getPosition().y <= 0) {
			gameOver = true;
		}
	}

	private void fire() {
		Bullet bullet = new Bullet(world, getX() + width / 5 * 4, getY() + 2.0f);
		bullets.add(bullet);
		getStage().addActor(bullet);
		bullet.move(this.getX(), 1000);
	}
}
