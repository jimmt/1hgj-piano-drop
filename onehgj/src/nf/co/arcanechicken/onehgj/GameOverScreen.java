package nf.co.arcanechicken.onehgj;

import com.badlogic.gdx.scenes.scene2d.ui.Table;

public class GameOverScreen extends AbstractScreen {

	public GameOverScreen(OneHGJ sg) {
		super(sg);
		Table table = super.getTable();
		table.add("GAME OVER");
	}

}
