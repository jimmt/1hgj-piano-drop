package nf.co.arcanechicken.onehgj;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public class Obstacle extends Image{
	private Body body;
	private float width = 2f, height = 2f;
	public Obstacle(World world){
		super(Textures.piano);
		
		PolygonShape hitbox = new PolygonShape();
		hitbox.setAsBox(width / 2, height / 2);

		BodyDef bd = new BodyDef();
		bd.type = BodyType.DynamicBody;
		bd.fixedRotation = true;

		body = world.createBody(bd);

		FixtureDef fd = new FixtureDef();
		fd.shape = hitbox;
		fd.friction = 10.0f;
		fd.density = 1.0f;
		body.createFixture(fd);
		body.setUserData(this);
		body.setTransform(MathUtils.random() * 6, Constants.UNIT_HEIGHT, 0);

		setPosition(body.getPosition().x - width / 2, body.getPosition().y - height / 2);

		setSize(width, height);
	}
	public void act(float delta){
		super.act(delta);
		setPosition(body.getPosition().x - width / 2, body.getPosition().y - height / 2);
	}
}
