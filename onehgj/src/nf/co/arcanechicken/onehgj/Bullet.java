package nf.co.arcanechicken.onehgj;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public class Bullet extends Image {
	Body body;
	private float width = 0.1f, height = 0.3f;

	public Bullet(World world, float x, float y) {
		super(Textures.bullet);

		PolygonShape hitbox = new PolygonShape();
		hitbox.setAsBox(width / 2, height / 2);

		BodyDef bd = new BodyDef();
		bd.type = BodyType.DynamicBody;
		bd.fixedRotation = true;

		body = world.createBody(bd);

		FixtureDef fd = new FixtureDef();
		fd.shape = hitbox;
		fd.friction = 10.0f;
		fd.density = 100.0f;
		body.createFixture(fd);
		body.setUserData(this);
		body.setTransform(0, 1.0f, 0);

		body.setTransform(x, y, 0);

		setPosition(body.getPosition().x - width / 2, body.getPosition().y - height / 2);

		setSize(width, height);
	}

	public void move(float x, float y) {
		body.setLinearVelocity(0, 50.0f);
	}

	public void act(float delta) {
		setPosition(body.getPosition().x - width / 2, body.getPosition().y - height / 2);
	}
}
