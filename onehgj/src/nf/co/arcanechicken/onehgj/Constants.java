package nf.co.arcanechicken.onehgj;

import com.badlogic.gdx.Gdx;

public class Constants {
	public static int WIDTH = Gdx.graphics.getWidth();
	public static int HEIGHT = Gdx.graphics.getHeight();
	public static float scale = 1/80f;
	public static float UNIT_WIDTH = WIDTH * scale;
	public static float UNIT_HEIGHT = HEIGHT * scale;
}
