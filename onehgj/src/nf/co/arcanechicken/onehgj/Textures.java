package nf.co.arcanechicken.onehgj;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

public class Textures {
	public static Texture player = new Texture(Gdx.files.internal("player.png"));
	public static Texture bullet = new Texture(Gdx.files.internal("bullet.png"));
	public static Texture piano = new Texture(Gdx.files.internal("piano.png"));
}
