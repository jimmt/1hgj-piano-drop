package nf.co.arcanechicken.onehgj;


public class GameScreen extends AbstractScreen {
	private Player player;
	private float lastSpawn = 99999f, spawnCap = 1.0f;

	public GameScreen(OneHGJ sg) {
		super(sg);
		
	}
	public void show(){
		super.show();
		
		stage.setViewport(Constants.UNIT_WIDTH, Constants.UNIT_HEIGHT, false);
		player = new Player(world);
		stage.addActor(player);
		
	}
	public void render(float parentAlpha){
		super.render(parentAlpha);
		
		if(lastSpawn > spawnCap){
		stage.addActor(new Obstacle(world));
		lastSpawn = 0f;
		} else {
			lastSpawn += 0.016f;
		}
		if(player.gameOver){
			sg.setScreen(new GameOverScreen(sg));
		}
		
	}
	

}
