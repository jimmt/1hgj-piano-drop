package nf.co.arcanechicken.onehgj;

import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class MenuScreen extends AbstractScreen {

	public MenuScreen(OneHGJ sg) {
		super(sg);
		
		
	}
	public void show(){
		super.show();
		
		Table table = super.getTable();
		table.setFillParent(true);
		table.setTransform(true);
		table.add("Piano Drop 1HGJ");
		
		table.row();
		
		TextButton start = new TextButton("Start", getSkin());
		table.add(start).width(Constants.WIDTH * 1 / 4);
		
		start.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y){
				//load savegames
				sg.setScreen(new GameScreen(sg));
			}
		});
		
	}

}
